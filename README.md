# Angular Material Dashboard

[![pipeline status](https://gitlab.com/angular-material-dashboard/angular-material-dashboard/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/angular-material-dashboard/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/62ac02e0a2e64ac09054046c2d821690)](https://www.codacy.com/app/angular-material-dashboard/angular-material-dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/angular-material-dashboard&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/62ac02e0a2e64ac09054046c2d821690)](https://www.codacy.com/app/angular-material-dashboard/angular-material-dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-dashboard/angular-material-dashboard&utm_campaign=Badge_Coverage)


One of the common SPAs is dashboard where users are able to controll the application, site, etc. This
project aim to handle basics fo a dashboard app.

## Install

This is an bower package, so you can install with the bower cli tool:

	bower install --save angular-material-dashboard

or address directly

	bower install --save https://gitlab.com/angular-material-dashboard/angular-material-dashboard.git
	
## Getting start

Addin panel directive into the index.html:

	<body>
		<div ui-view layout="row" layout-fill></div>
		<!-- Add javascripts -->
		...
	</body>



Read more about [api](https://angular-material-dashboard.gitlab.io/angular-material-dashboard/doc/index.html)
